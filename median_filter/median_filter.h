
/* to define:
   MFprefix (ex: mf)
   MFtype (ex: int16_t)
   MFlen (ex: 101)
   MFidxtype (MFlen must be coded on MFidxtype) (ex: uint8_t)
*/


#ifdef CONCATi
#   undef CONCATi
#endif

#ifdef CONCAT2
#   undef CONCAT2
#endif

#ifdef CONCAT3
#   undef CONCAT3
#endif

#define CONCATi(a,b)  a ## b
#define CONCAT2(a,b)  CONCATi(a,b)
#define CONCAT3(a,b,c) CONCAT2(a,CONCAT2(b,c))

#define _MF_circbuf CONCAT2(MFprefix,_circbuf)
#define _MF_circidx CONCAT2(MFprefix,_circidx)

#define _MF_fill CONCAT2(MFprefix,_fill)
void _MF_fill(MFtype);

#define _MF_update CONCAT2(MFprefix,_update)
void _MF_update(MFtype);

#define _MF_read CONCAT2(MFprefix,_read)
MFtype _MF_read();

#ifndef _LIB_MF_C_CODE
#   undef MFprefix
#   undef MFtype
#   undef MFlen
#   undef MFidxtype
#   undef _MF_circbuf
#   undef _MF_circidx
#   undef _MF_update
#   undef _MF_read
#endif
