
volatile MFtype _MF_circbuf[MFlen];
volatile MFidxtype _MF_circidx;

void _MF_fill(MFtype value)
{
    for(_MF_circidx = 0; _MF_circidx<MFlen; _MF_circidx++)
        _MF_circbuf[_MF_circidx] = value;
    _MF_circidx=0;
}

void _MF_update(MFtype value)
{
    _MF_circbuf[_MF_circidx] = value;
    _MF_circidx++;
    _MF_circidx%=MFlen;
}

MFtype _MF_read()
{
    MFtype temp[MFlen];

    MFidxtype i;
    for(i=0; i<MFlen; i++)
        temp[i] = _MF_circbuf[i];


    MFidxtype j;

    for(i=0; i<=MFlen/2; i++)
    {
        for(j=i+1; j<MFlen; j++)
        {
            if(temp[i]>temp[j])
            {
                MFtype swap = temp[i];
                temp[i] = temp[j];
                temp[j] = swap;
            }
        }
    }

    MFtype ret = temp[MFlen/2];
    return(ret);
}

