#include <util/delay.h>
#include <util/crc16.h>

/* To define
   DS18B20port (Ex: PORTF)
   DS18B20pin (Ex: PIN0_bm)
   DS12B20prefix (Ex: thermometer)
*/

//commands
#define DS18B20_CMD_CONVERTTEMP 0x44
#define DS18B20_CMD_RSCRATCHPAD 0xbe
#define DS18B20_CMD_WSCRATCHPAD 0x4e
#define DS18B20_CMD_CPYSCRATCHPAD 0x48
#define DS18B20_CMD_RECEEPROM 0xb8
#define DS18B20_CMD_RPWRSUPPLY 0xb4
#define DS18B20_CMD_SEARCHROM 0xf0
#define DS18B20_CMD_READROM 0x33
#define DS18B20_CMD_MATCHROM 0x55
#define DS18B20_CMD_SKIPROM 0xcc
#define DS18B20_CMD_ALARMSEARCH 0xec

#ifdef CONCATi
#   undef CONCATi
#endif

#ifdef CONCAT2
#   undef CONCAT2
#endif

#ifdef CONCAT3
#   undef CONCAT3
#endif

#define CONCATi(a,b)  a ## b
#define CONCAT2(a,b)  CONCATi(a,b)
#define CONCAT3(a,b,c) CONCAT2(a,CONCAT2(b,c))

#define _DS_reset CONCAT2(DS18B20prefix,_reset)
uint8_t _DS_reset();

#define _DS_writebit CONCAT2(DS18B20prefix,_writebit)
void _DS_writebit(uint8_t bit);

#define _DS_readbit CONCAT2(DS18B20prefix,_readbit)
uint8_t _DS_readbit(void);

#define _DS_writebyte CONCAT2(DS18B20prefix,_writebyte)
void _DS_writebyte(uint8_t byte);

#define _DS_readbyte CONCAT2(DS18B20prefix,_readbyte)
uint8_t _DS_readbyte(void);

#define _DS_gettemp CONCAT2(DS18B20prefix,_gettemp)
int8_t _DS_gettemp(int16_t* temp, uint16_t timeout_ms);

#ifndef _LIB_DS18B20_C_CODE
#   undef DS18B20port
#   undef DS18B20pin
#   undef DS18B20prefix
#   undef DS18B20_CMD_CONVERTTEMP
#   undef DS18B20_CMD_RSCRATCHPAD
#   undef DS18B20_CMD_WSCRATCHPAD
#   undef DS18B20_CMD_CPYSCRATCHPAD
#   undef DS18B20_CMD_RECEEPROM
#   undef DS18B20_CMD_RPWRSUPPLY
#   undef DS18B20_CMD_SEARCHROM
#   undef DS18B20_CMD_READROM
#   undef DS18B20_CMD_MATCHROM
#   undef DS18B20_CMD_SKIPROM
#   undef DS18B20_CMD_ALARMSEARCH
#   undef _DS_reset
#   undef _DS_writebit
#   undef _DS_readbit
#   undef _DS_readbyte
#   undef _DS_writebyte
#   undef _DS_gettemp
#endif
