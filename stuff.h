
#define float_to_int32(f,scale) ((int32_t)(f*(1L<<scale)+.5))
#define int32_to_float(i,scale) (((float)i)/(1L<<scale))
