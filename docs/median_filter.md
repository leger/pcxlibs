# Compile the lib

## fichiers .h et .c

Créer `<prefix>.h`:

```
#ifndef _H_<prefix>
#define _H_<prefix>

#include "common.h"

#define MFprefix <prefix>
#define MFtype <type>
#define MFlen <len>
#define MFidxtype <idxtype>

#include "<libdir>/median_filter/median_filter.h"

#endif
```

Et `<prefix>.c`:

```
#define _LIB_MF_C_CODE 1
#include "<prefix>.h"

#include "<libdir>/median_filter/median_filter.c"
```

Add `#include "<prefix>.h"` in `common.h`.

Add `<prefix>.o` in `Makefile`.


# Use the lib

## Somewhere:

```
mf_fill(smth);
mf_update(smth);
smth = mf_read();
```
