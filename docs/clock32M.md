# Compile the lib

```
ln -s LIBDIR/clock/clock32M.c .
ln -s LIBDIR/clock/clock32M.h .
```

Add `#include "clock32M.h"` in `common.h`.

Add `clock32M.o` in `Makefile`.


# Use the lib

## In main:

```
clock32M_init();
```
