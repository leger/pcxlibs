# Compile the lib

## fichiers .h et .c

Créer `<prefix>.h`:

```
#ifndef _H_<prefix>
#define _H_<prefix>

#include "common.h"

#define USARTname <ex: C1>
#define USARTport <ex: PORTC>
#define USARTtxpin <ex: PIN7_bm>
#define USARTbaudrate <ex: 9600>
#define USARTprefix <ex: debug>
#define USARTtxen <ex: true>
#define USARTrxen <ex: true>
#define USARTrxhandler <ex: get_debug>
#include "<libdir>/serial/uart.h"

void get_debug(const char* msg);

#endif
```

Et `<prefix>.c`:

```
#define _LIB_UART_C_CODE 1
#include "<prefix>.h"

#include "<libdir>/serial/uart.c"

void get_debug(const char* msg)
{}
```

Add `#include "<prefix>.h"` in `common.h`.

Add `<prefix>.o` in `Makefile`.


# Use the lib

## Somewhere:

```
<prefix>_init();
<prefix>_putchar('\n');
<prefix>_putzchar("toto");
<prefix>_putfloat(12.4, 4);
<prefix>_printf("toto %u", 24);
```
