#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>

typedef struct rs485msg_struct
{
    char from;
    char to;
    char content[256];
} rs485msg_t;


/* to define

   F_CPU
   USARTname (ex: C0)
   USARTport (ex: PORTC)
   USARTtxpin  (ex: PIN3_bm)
   USARTdeport (ex: PORTx)
   USARTdepin (ex: PINx_bm)
   USARTprefix (ex: debug)
   USARTtxen (default: false)
   USARTrxen (default: false)
   USARTrxlen (default: 256)
   USARTtxlen (default: 256)
   USARTbaudrate (default: 9600)
   USARTchsize (default: 8)
   USARTrxtriggerbegin (default 0x01 a.k.a SOH)
   USARTrxtriggerend (default 0x04 a.k.a EOT)
   USARTrxhandler (function name called with the zchar)
*/

#define CONCATi(a,b)  a ## b
#define CONCAT2(a,b)  CONCATi(a,b)
#define CONCAT3(a,b,c) CONCAT2(a,CONCAT2(b,c))

#define _USART CONCAT2(USART,USARTname)

#ifndef USARTtxlen
#   define USARTtxlen 256
#endif

#ifndef USARTrxlen
#   define USARTrxlen 256
#endif

#ifndef USARTbaudrate
#   define USARTbaudrate 9600
#endif

#ifndef USARTchsize
#   define USARTchsize 8
#endif

#ifndef USARTrxtriggerbegin
#   define USARTrxtriggerbegin 0x01
#endif

#ifndef USARTrxtriggerend
#   define USARTrxtriggerend 0x04
#endif

#ifndef USARTtxen
#   define USARTtxen false
#endif

#ifndef USARTrxen
#   define USARTrxen false
#endif


#if USARTtxen == true
#   define _USART_txbuffer CONCAT2(USARTprefix,_txbuffer)
#   define _USART_txidxA CONCAT2(USARTprefix,_txidxA)
#   define _USART_txidxB CONCAT2(USARTprefix,_txidxB)
#endif

#if USARTrxen == true
#   define _USART_rxbuffer CONCAT2(USARTprefix,_rxbuffer)
#   define _USART_rxidx CONCAT2(USARTprefix,_rxidx)
#endif

#define _USART_init CONCAT2(USARTprefix,_init)

void _USART_init();

#if USARTrxen == true
#   define _USART_RXC_vect CONCAT3(USART,USARTname,_RXC_vect)
#   define _USART_rxmsg CONCAT2(USARTprefix, _rxmsg)
void _USART_rxmsg();
#endif
    
#if USARTtxen == true
#   define _USART_DRE_vect CONCAT3(USART,USARTname,_DRE_vect)
#   define _USART_TXC_vect CONCAT3(USART,USARTname,_TXC_vect)
#   define _USART_sendmsg CONCAT2(USARTprefix, _sendmsg)
#   define _USART_putchar CONCAT2(USARTprefix, _putchar)
#   define _USART_putzchar CONCAT2(USARTprefix, _putzchar)
#   define _USART_putfloat CONCAT2(USARTprefix, _putfloat)
#   define _USART_printf CONCAT2(USARTprefix, _printf)
void _USART_sendmsg(rs485msg_t* m);
void _USART_putchar(uint8_t ch);
void _USART_putzchar(const char* zchar);
void _USART_putfloat(const float f, uint8_t decsize);
void _USART_printf(const char *fmt, ...);
#endif


#ifndef _LIB_RS485_C_CODE
#   undef USARTname
#   undef USARTport
#   undef USARTtxpin
#   undef USARTprefix
#   undef USARTtxen
#   undef USARTrxen
#   undef USARTrxlen
#   undef USARTtxlen
#   undef USARTbaudrate
#   undef USARTchsize
#   undef USARTrxtrigger
#   undef USARTrxhandler
#   undef USARTrxbytenotify
#endif
