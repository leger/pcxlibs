
#if USARTtxen == true
    volatile uint8_t _USART_txbuffer[USARTtxlen];
    volatile uint16_t _USART_txidxB;
    volatile uint16_t _USART_txidxA;
#endif

#if USARTrxen == true
    volatile uint8_t _USART_rxbuffer[USARTrxlen];
    volatile uint16_t _USART_rxidx;
#endif

void _USART_init()
{
    USARTport.DIRSET = USARTtxpin;
    USARTport.OUTSET = USARTtxpin;
    USARTdeport.DIRSET = USARTdepin;
    USARTdeport.OUTCLR = USARTdepin;

    uint16_t bscale = 0;
    if(USARTbaudrate<600)
        bscale++;
    if(USARTbaudrate<300)
        bscale++;
    if(USARTbaudrate<150)
        bscale++;
    if(USARTbaudrate<75)
        bscale++;
    if(USARTbaudrate<36)
        bscale++;
    if(USARTbaudrate<18)
        bscale++;
    if(USARTbaudrate<9)
        bscale++;

    uint16_t bsel = (F_CPU-((((uint32_t)8)<<bscale) * USARTbaudrate)) / ((((uint32_t)16)<<bscale) * USARTbaudrate);

    _USART.BAUDCTRLA = bsel;
    _USART.BAUDCTRLB = (bscale << USART_BSCALE_gp) | (bsel >> 8);

    _USART.CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc;
    if(USARTchsize==5)
        _USART.CTRLC |= USART_CHSIZE_5BIT_gc;
    else if(USARTchsize==6)
        _USART.CTRLC |= USART_CHSIZE_6BIT_gc;
    else if(USARTchsize==7)
        _USART.CTRLC |= USART_CHSIZE_7BIT_gc;
    else if(USARTchsize==8)
        _USART.CTRLC |= USART_CHSIZE_8BIT_gc;
    else
        _USART.CTRLC |= USART_CHSIZE_9BIT_gc;
    _USART.CTRLA = (USARTrxen?USART_RXCINTLVL_MED_gc:USART_RXCINTLVL_OFF_gc);
    _USART.CTRLA|= (USARTtxen?USART_TXCINTLVL_MED_gc:0x00);
    _USART.CTRLB = (USARTrxen?USART_RXEN_bm:0x00) | (USARTtxen?USART_TXEN_bm:0x00);
}

#if USARTrxen == true
ISR(_USART_RXC_vect)
{
    uint8_t received = _USART.DATA;
#ifdef USARTrxbytenotify
    USARTrxbytenotify();
#endif
    if(received==USARTrxtriggerend)
    {
        _USART_rxbuffer[_USART_rxidx]=0;
        _USART_rxmsg();
        _USART_rxidx=0;
    }
    else
    {
        if(received==USARTrxtriggerbegin)
        {
            _USART_rxidx=0;
        }
        else
        {
            _USART_rxbuffer[_USART_rxidx] = received;
            _USART_rxidx++;
            if(_USART_rxidx>USARTrxlen)
                _USART_rxidx=0;
        }
    }

}

char snip_to_char(uint8_t snip)
{
    if(snip<10)
        return snip+'0';
    return snip+'a'-10;
}

void uint16_to_4char(uint16_t nb, char* c)
{
    c[0] = snip_to_char((nb>>12) & 0x0F);
    c[1] = snip_to_char((nb>>8)  & 0x0F);
    c[2] = snip_to_char((nb>>4)  & 0x0F);
    c[3] = snip_to_char( nb      & 0x0F);
}

void _USART_rxmsg()
{
    uint16_t crc = 0xFFFF;
    uint16_t k=0;
    while(_USART_rxbuffer[k]!=0)
    {
        k++;
    }

    uint16_t msglen = k-4;

    for(k=0;k<msglen;k++)
        crc = _crc_ccitt_update(crc, _USART_rxbuffer[k]);

    char c[4];
    uint16_to_4char(crc,c);
    
    if(
            (c[0]==_USART_rxbuffer[msglen]) &&
            (c[1]==_USART_rxbuffer[msglen+1]) &&
            (c[2]==_USART_rxbuffer[msglen+2]) &&
            (c[3]==_USART_rxbuffer[msglen+3])
      )
    {
        if((_USART_rxbuffer[2] == 0x02) && (_USART_rxbuffer[msglen-1] == 0x03))
        {
            rs485msg_t msg;
            msg.from = _USART_rxbuffer[0];
            msg.to = _USART_rxbuffer[1];
            for(k=3;k<msglen-1;k++)
                msg.content[k-3]=_USART_rxbuffer[k];
            msg.content[k-3] = 0;
            USARTrxhandler(&msg);
        }
        else
        {
            debug_putzchar("RS485: ERROR: valid CRC, invalid frame\n");
        }
    }
    else
    {
        debug_printf("RS485: ERROR: invalid CRC, received=\"%c%c%c%c\", computed=\"%c%c%c%c\"\n", 
                _USART_rxbuffer[msglen],
                _USART_rxbuffer[msglen+1],
                _USART_rxbuffer[msglen+2],
                _USART_rxbuffer[msglen+3],
                c[0],
                c[1],
                c[2],
                c[3]);
    }
}

#endif

#if USARTtxen == true
ISR(_USART_DRE_vect)
{
    USARTdeport.OUTSET = USARTdepin;
    if(_USART_txidxA == _USART_txidxB)
    {
        // empty
        _USART.CTRLA = (_USART.CTRLA & ~USART_DREINTLVL_gm) | USART_DREINTLVL_OFF_gc;
    }
    else
    {
        _USART.DATA = _USART_txbuffer[_USART_txidxA];
        _USART_txidxA++;
        _USART_txidxA%=USARTtxlen;
    }
}

ISR(_USART_TXC_vect)
{
    USARTdeport.OUTCLR = USARTdepin;
}

void _USART_putchar(uint8_t ch)
{
    while((_USART_txidxB+1)%USARTtxlen == _USART_txidxA);

    _USART_txbuffer[_USART_txidxB] = ch;
    _USART_txidxB++;
    _USART_txidxB%=USARTtxlen;

    _USART.CTRLA = (_USART.CTRLA & ~USART_DREINTLVL_gm) | USART_DREINTLVL_MED_gc;
}

void _USART_sendmsg(rs485msg_t* msg)
{
    debug_printf("send to [%c]: [%s]\n",msg->to, msg->content);
    char zmsg[256];
    zmsg[0]=0x01;
    zmsg[1]=0x01;
    zmsg[2]=msg->from;
    zmsg[3]=msg->to;
    zmsg[4]=0x02;
    uint16_t k=0;
    while(msg->content[k]!=0)
    {
        zmsg[5+k] = msg->content[k];
        k++;
    }
    zmsg[5+k]=0x03;

    uint16_t msglen = 5+k+1;
    uint16_t crc = 0xFFFF;
    for(k=2;k<msglen;k++)
        crc = _crc_ccitt_update(crc, zmsg[k]);

    uint16_to_4char(crc, zmsg+msglen);
    zmsg[msglen+4]=0x04;
    zmsg[msglen+5]=0;

    for(k=0;k<msglen+5;k++)
        _USART_putchar(zmsg[k]);
}

void _USART_putzchar(const char* zchar)
{
    uint16_t i=0;
    while(zchar[i]!=0)
    {
        _USART_putchar(zchar[i]);
        i++;
    }
}

void _USART_putfloat(const float f, uint8_t decsize)
{
    int32_t fl;

    char buf[64];
    
    if(decsize>0)
    {
        fl=f;
        float decimal = f-fl;
        if(decimal<0)
            decimal*=-1;
        
        char bufformat[64];
        uint8_t i;
        for(i=0;i<decsize;i++)
            decimal*=10;
        int16_t n;
        n = snprintf(bufformat,64,"%%li.%%0%ulu",decsize);
        if(n<64)
        {
            n = snprintf(buf,64,bufformat,fl,(uint32_t)(decimal+.5));
            if(n<63)
                _USART_putzchar(buf);
        }
    }
    else
    {
        if(f>=0)
            fl = f+.5;
        else
            fl = f-.5;

        int16_t n = snprintf(buf,64,"%li.",fl);
        if(n<63)
            _USART_putzchar(buf);
    }
}


void _USART_printf(const char *fmt, ...)
{
    char buf[USARTtxlen];

    va_list ap;
    va_start(ap, fmt);
    int16_t n = vsnprintf(buf, USARTtxlen, fmt, ap);
    va_end(ap);

    if (n < USARTtxlen)
        _USART_putzchar(buf);
}

#endif
